<?php

use \zaporylie\Tripletex\Client\Token\TokenStorageInterface;
use \zaporylie\Tripletex\Model\Token\SessionToken;

/**
 * Class TripletexDrupalUserStorage
 */
class TripletexApiDrupalUserStorage implements TokenStorageInterface
{

  /**
   * User on which token must be stored.
   *
   * @var object
   */
  protected $user;

  /**
   * TripletexDrupalUserStorage constructor.
   *
   * @param object $user
   */
  public function __construct($user) {
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    if (!$this->has()) {
      throw new LogicException('Missing token');
    }
    return $this->user->data['tripletex_api']['token'];
  }

  /**
   * {@inheritdoc}
   */
  public function set(SessionToken $token) {
    $this->user->data['tripletex_api']['token'] = $token;
    user_save($this->user);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function has() {
    return isset($this->user->data['tripletex_api']['token']) && $this->user->data['tripletex_api']['token'] instanceof SessionToken;
  }

  /**
   * {@inheritdoc}
   */
  public function clear() {
    $this->user->data['tripletex_api']['token'] = NULL;
    user_save($this->user);
    return $this;
  }

}
