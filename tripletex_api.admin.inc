<?php

/**
 * Config form.
 */
function tripletex_api_settings_form($form, &$form_state) {
  $form['tripletex_api_customer_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer token'),
    '#required' => TRUE,
    '#default_value' => variable_get('tripletex_api_customer_token'),
  );

  $form['tripletex_api_session_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Session length'),
    '#description' => t('Time in seconds.'),
    '#default_value' => variable_get('tripletex_api_session_length', 604800),
  );

  $form['tripletex_api_privileged_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Privileged user'),
    '#default_value' => variable_get('tripletex_api_privileged_user'),
    '#description' => t('User is for the user with privileged access to Tripletex API'),
  );
  $form = system_settings_form($form);
  return $form;
}

/**
 * User admin page.
 *
 * @param $user
 *
 * @return array
 */
function tripletex_api_user_page($user) {
  $build = array();

  // Get client.
  $tripletex = tripletex_api_get_app($user);

  if ($tripletex) {
    /** @var \zaporylie\Tripletex\Model\Token\SessionToken $token */
    $token = $user->data['tripletex_api']['token'];

    $build['session_token'] = array(
      '#type' => 'item',
      '#title' => t('Session Token'),
      '#markup' => $token->getToken(),
    );
    $build['session_expire'] = array(
      '#type' => 'item',
      '#title' => t('Expire'),
      '#markup' => $token->getExpirationDate()->format('c'),
    );
    $build['session_autorenew'] = array(
      '#type' => 'item',
      '#title' => t('Autorenewal'),
      '#markup' => t('Yes'),
    );
    $build['form'] = drupal_get_form('tripletex_api_disassociate_form', $user);
  }
  else {
    $build['session'] = array(
      '#markup' => t('No session found'),
    );
    $build['form'] = drupal_get_form('tripletex_api_employee_token_form', $user);
  }

  return $build;
}

/**
 * User token form.
 */
function tripletex_api_employee_token_form($form, &$form_state, $user) {

  $form_state['#entity'] = $user;

  $form['employee_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Employee token'),
    '#required' => TRUE,
    '#default_value' => isset($user->data['tripletex_api']['employee_token']) ? $user->data['tripletex_api']['employee_token'] : NULL,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * User token form: submit handler.
 */
function tripletex_api_employee_token_form_submit($form, &$form_state) {
  $user = $form_state['#entity'];
  $user->data['tripletex_api']['employee_token'] = $form_state['values']['employee_token'];
  user_save($user);
  drupal_set_message(t('Token saved'));
}

/**
 * User disassociate form.
 */
function tripletex_api_disassociate_form($form, &$form_state, $user) {
  $form_state['#entity'] = $user;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Revoke access'),
  );

  return $form;
}

/**
 * User disassociate form: submit handler.
 */
function tripletex_api_disassociate_form_submit($form, &$form_state) {
  $user = $form_state['#entity'];

  if ($tripletex = tripletex_api_get_app($user)) {
    if (isset($user->data['tripletex_api']['token'])
      && $user->data['tripletex_api']['token'] instanceof \zaporylie\Tripletex\Model\Token\SessionToken) {
      /** @var \zaporylie\Tripletex\Model\Token\SessionToken $token */
      $token = $user->data['tripletex_api']['token'];
      $tripletex->session()->delete($token->getToken());
    }
    $tripletex->getClient()->getSessionToken()->clear();
  }

  $user->data['tripletex_api'] = array();
  user_save($user);
  drupal_set_message(t('API access revoked'));
}
